/*
* Copyright 2019 
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http ://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef __SOP_FbxAnimation_h__
#define __SOP_FbxAnimation_h__

#include <SOP/SOP_Node.h>
#include <UT/UT_StringHolder.h>
#include <fbxsdk.h>


class FbxArchive
{
public:
	FbxArchive();
	~FbxArchive();

	bool open(const char* filename);
    bool isOpen(const char* filename);
	void close();
	FbxScene* getScene();
private:
	std::string filename_;
	FbxManager * manager_;
	FbxScene* scene_;
};

class SOP_FbxAnimation : public SOP_Node
{
public:
    static PRM_Template *buildTemplates();
    static OP_Node *myConstructor(OP_Network *net, const char *name, OP_Operator *op)
    {
        return new SOP_FbxAnimation(net, name, op);
    }

    virtual const SOP_NodeVerb *cookVerb() const override;
    FbxArchive& fbxArchive() { return fbxar; }

	static const UT_StringHolder theSOPTypeName;
protected:
    SOP_FbxAnimation(OP_Network *net, const char *name, OP_Operator *op)
        : SOP_Node(net, name, op)
    {
        mySopFlags.setManagesDataIDs(true);
    }
    
    virtual ~SOP_FbxAnimation() 
    {
    }

    virtual OP_ERROR cookMySop(OP_Context &context) override
    {
        return cookMyselfAsVerb(context);
    }
private:
    FbxArchive fbxar;
};

#endif
